from src.main import sum_of_two

def test_sum_two():
    result = sum_of_two(2,5)
    assert result == 7
